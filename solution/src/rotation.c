

#include "../include/rotation.h"

struct image rotate_image(struct image source) {
    struct image new_image = {.width = source.height, .height = source.width, NULL};
    new_image.data = malloc(new_image.width * new_image.height * sizeof(struct pixel));
    for (size_t i = 0; i < source.width; i++) {
        for (size_t j = 0; j < source.height; j++) {
            new_image.data[i*new_image.width + (new_image.width-j-1)] = source.data[j*source.width + i];
        }
    }
    return new_image;
}
