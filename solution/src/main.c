#include "../include/bmp.h"
#include "../include/openClose.h"
#include "../include/rotation.h"
#include <stdio.h>
#include <stdlib.h>

int main( int argc, char** argv ) {

    if (argc != 3 ) {
        fprintf(stderr, "The amount of arguments must be 3\n");
        exit(1);
    }

    FILE *input = NULL;
    FILE *output = NULL;
    struct image i1 = {0};
    struct image i2 = {0};

    open_file(&input, argv[1], "rb");

    if (from_bmp(input, &i1) != READ_OK) {
        fprintf(stderr, "Error occurred while reading");
        exit(1);
    }

     i2 = rotate_image(i1);


    open_file(&output, argv[2], "wb");

    if (to_bmp(output, &i2) != WRITE_OK) {
        fprintf(stderr, "Error occurred while writing");
        exit(1);
    }

    close_file(&output);
    close_file(&input);

    free(i2.data);
    free(i1.data);
    return 0;
}
