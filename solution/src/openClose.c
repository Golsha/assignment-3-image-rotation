//
// Created by User on 13.12.2022.
//


#include "../include/openClose.h"

void open_file(FILE** fp, char*  path, char* mode) {
    *fp = fopen(path, mode);
    if (!*fp) {
        printf("Error occurred while opening the file, %s",path);
        exit(1);
    }
}

void close_file(FILE** fp) {
    if (!*fp) {
        printf("Error occurred while closing the file");
        exit(1);
    }
    fclose(*fp);
}

