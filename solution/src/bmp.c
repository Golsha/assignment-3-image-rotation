#include "../include/bmp.h"

static const uint16_t ID = 19778;
static const uint32_t PIXEL_OFFSET = 54;
static const uint32_t UNUSED = 0;
static const uint32_t HEADER_SIZE = 40;
static const uint16_t COLOR_PLANES = 1;
static const uint16_t BITS_PER_PIXEL = 24;
static const uint32_t COMPRESSION = 0;
static const uint32_t COLORS_COUNT = 0;
static const uint32_t IMP_COLORS_COUNT = 0;

static const uint16_t PROPORTION = 3;



size_t get_padding(size_t size){
    size_t reminder = size % 4;
    if (reminder == 0){
        return 0;
    }
    return 4 - reminder;
}

enum read_status from_bmp( FILE* in, struct image* img ){
    struct bmp_header bmp_header = {0};
    if (!fread(&bmp_header, sizeof (struct bmp_header),1,in)){
        return READ_INVALID_HEADER;
    }
    img->height = bmp_header.biHeight;
    img->width = bmp_header.biWidth;
    size_t padding = get_padding(img->width * sizeof (struct pixel));

    img->data= malloc(img->width * img->height * sizeof (struct pixel));
    for (size_t i = 0; i<bmp_header.biHeight;i++){

        if (fread(img->data + i*img->width,sizeof (struct pixel),img->width,in)!= img->width){
            return READ_INVALID_BITS;
        }
        if (fseek(in,(long)padding,SEEK_CUR)){
            return READ_INVALID_BITS;
        }
    }
    return READ_OK;
}



// fix magic const

void init_headers(struct bmp_header* bmp_header, struct image* image ){
    size_t padding = get_padding(image->width * sizeof (struct pixel));
    size_t size_of_file = image->height * (image->width * PROPORTION + padding);
    *bmp_header = (struct bmp_header) {
            .bfType = ID,
            .bfileSize = PIXEL_OFFSET + size_of_file,
            .bfReserved = UNUSED,
            .bOffBits = PIXEL_OFFSET,
            .biSize = HEADER_SIZE,
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = COLOR_PLANES,
            .biBitCount = BITS_PER_PIXEL,
            .biCompression = COMPRESSION,
            .biSizeImage = size_of_file,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = COLORS_COUNT,
            .biClrImportant = IMP_COLORS_COUNT
    };
}

enum write_status to_bmp( FILE* out, struct image* img ){
    size_t padding = get_padding(img->width * sizeof (struct pixel));
    struct bmp_header bmp_header = {0};
    init_headers(&bmp_header, img);
    if (!fwrite(&bmp_header, sizeof(struct bmp_header), 1, out)) {
        return WRITE_ERROR;
    }

    const int32_t* zero = NULL;
    for (size_t i = 0; i < img->height; i++) {
        if (fwrite(img->data + i*img->width, sizeof(struct pixel), img->width, out) != img->width)
            return WRITE_ERROR;
        if (fwrite(&zero, 1, padding, out) != padding)
            return WRITE_ERROR;
    }

    return WRITE_OK;
}

