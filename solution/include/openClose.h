

#ifndef IMAGE_TRANSFORMER_OPENCLOSE_H
#define IMAGE_TRANSFORMER_OPENCLOSE_H

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

void open_file(FILE** fp, char*  path, char* mode);
void close_file(FILE** fp);

#endif //IMAGE_TRANSFORMER_OPENCLOSE_H
