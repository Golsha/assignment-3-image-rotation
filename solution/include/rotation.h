//
// Created by User on 13.12.2022.
//

#ifndef IMAGE_TRANSFORMER_ROTATION_H
#define IMAGE_TRANSFORMER_ROTATION_H
#include "../include/image.h"
#include "../include/rotation.h"
#include <malloc.h>
struct image rotate_image(struct image source);
#endif //IMAGE_TRANSFORMER_ROTATION_H
